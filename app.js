const Towxml = require('./towxml/main');
App({
    towxml: new Towxml(),
    onLaunch: function() {
        wx.request({
            url: this.globalData.ajaxUrl + 'member/up_token',
            header: {
                'content-type': 'application/x-www-form-urlencoded',
            },
            method: 'POST',
            data: {
                token: wx.getStorageSync('token')
            },
            success: res=> {
                this.globalData.QINIU_IMG_URL = res.data.data.qiniu_doman
                
                console.log(res)
            },
            fail: function (res) {
                console.log(res);
                wx.showToast({
                    title: '网络开了下小差，再试一次吧',
                    icon: 'none',
                    duration: 2000
                })
            },
        });


    },
    globalData: {
        userInfo: null,
        ajaxUrl: 'https://ruiyuanhui.3todo.com/api/',
        QINIU_IMG_URL: "",
        qiniuToken: ''
    }
})