//Page Object
Page({
    data: {
        url: ''
    },
    //options(Object)
    onLoad(options) {
        console.log(options)
        if (options.url == undefined) {
            this.setData({
                url: 'https://umscard.sinoqy.com/umsgiftcard2/giftcard/ruikh/62/index.html'
            })
        } else {
            this.setData({
                url: decodeURIComponent(options.url)
            })
        }
        wx.showModal({
            title: 's',
            content: options.url,
        })
    },
    onShow: function() {

    },
    addCard() {
        wx.navigateTo({
            url: '/pages/my/addMemberCard/addMemberCard'
        });
    },
    onShareAppMessage() {
        // return {
        //     path: '/pages/my/memberCard/memberCard?url=' + encodeURIComponent('https://umscard.sinoqy.com/umsgiftcard2/giftcard/ruikh/62/togive.html?giftCardId=k4kxte2x6JRvUF&balance=1.00&from=singlemessage')
        // }
        
    }
});