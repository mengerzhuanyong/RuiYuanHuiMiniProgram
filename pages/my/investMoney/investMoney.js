// pages/my/investMoney/investMoney.js
import {
    ajaxPost,
    showToast
} from '../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        inputValue: ""
    },



    //获取输入的金额
    bindValue(e) {
        this.setData({
            inputValue: e.detail.value
        })
    },
    //微信支付
    wxPay() {
        if (this.data.inputValue == '' || this.data.inputValue.length == 0) {
            wx.showToast({
                title: "请输入金额",
                icon: "none"
            })
        } else {
            let data = {
                amount: this.data.inputValue
            }
            ajaxPost('weixin/pay', data, res => {
                if(res.code == 0){
                    wx.requestPayment({
                        timeStamp: res.data.timestamp,
                        nonceStr: res.data.nonceStr,
                        package: res.data.package,
                        signType: res.data.signType,
                        paySign: res.data.paySign,
                        success:res=>{
                            console.log(res)
                            ajaxPost('weixin/save_money', data, res => {
                                if (res.code == 0) {
                                    showToast('text', '您已支付成功！')
                                    setTimeout(() => {
                                        wx.navigateTo({
                                            url: '/pages/my/investSuccess/investSuccess?money=' + this.data.inputValue,
                                        })
                                    }, 1000)
                                } else if (res.code == 80001) {
                                    showToast('text', res.msg)
                                } else {
                                    showToast('text', res.msg)
                                }
                                
                            })
                            
                        },
                        fail:res=>{
                            console.log(res)
                        }
                    })
                }
            })
            
        }
    }
})