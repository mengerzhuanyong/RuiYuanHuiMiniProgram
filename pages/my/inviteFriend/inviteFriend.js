// pages/my/inviteFriend/inviteFriend.js
import {
    ajaxPost,
    showToast
} from '../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        id: '',
        qrcode:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        ajaxPost('member/index', {}, res => {
            var data = res.data.member;
            this.setData({
                id: data.id,
                qrcode: 'https://ruiyuanhui.3todo.com/api/Qrcode/test?share=true&id=' + data.id
            })
            
        })
    },


   

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {
        return {
            title: '邀请你进入瑞源汇小程序',
            path: '/pages/index/index/index?id=' + this.data.id+'&share=true',
            imageUrl: ''
        }
    },
       /**
     * 预览图片
     * @param {*} e 
     */
    previewImage(e){
        wx.previewImage({
            current: this.data.qrcode,
            urls: [this.data.qrcode]
        })
    }
})