// pages/my/investSuccess/investSuccess.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        money:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            money:options.money
        })
    },


   /**
     * 确定
     * @param {*} e 
     */
    confirm(){
        wx.switchTab({
            url: '/pages/scoresMall/index/index'
        });
          
    }
})