// pages/my/setAddress/create/create.js
import {
    ajaxPost,
    showToast
} from '../../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        addressList: [],
        ischoose: false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        if (options.ischoose) {
            this.setData({
                ischoose: options.ischoose
            })
        }
    },



    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        this.getAddressList()
    },



    //设置默认地址
    setDefaultAddress(e) {
        var index = e.currentTarget.dataset.index;
        var list = this.data.addressList;
        var id = e.currentTarget.dataset.id;
        var i = 0;
        for (i in list) {
            list[i].is_default = 0
        }
        list[index].is_default = 1;
        let data = {
            id: id,
            is_default: 1
        }
        ajaxPost("member/is_default", data, res => {
            if (res.code == 0) {
                showToast('text', res.msg)
                this.setData({
                    addressList: list
                })
            } else {
                showToast('text', res.msg)
            }
        })
    },
       /**
     * 添加地址
     * @param {*} e 
     */
    addNewAddress() {
        wx.navigateTo({
            url: '/pages/my/setAddress/edit/edit?add=true'
        })
    },
       /**
     * 获取地址列表
     * @param {*} e 
     */
    getAddressList() {
        ajaxPost('member/address', {}, res => {
            console.log(res)
            var data = res.data.dataList;
            this.setData({
                addressList: data
            })
        })
    },
       /**
     * 编辑地址
     * @param {*} e 
     */
    editAddress(e) {
        var id = e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '/pages/my/setAddress/edit/edit?id=' + id
        })
    },
       /**
     * 删除地址
     * @param {*} e 
     */
    deleteAdd(e) {
        var index = e.currentTarget.dataset.index;
        var id = e.currentTarget.dataset.id;
        var addressList = this.data.addressList;
        // if (addressList.length == 1){
        //     return showToast('text','默认地址不能删除')
        // }
        let data = {
            id: id
        }
        
        wx.showModal({
            title: '提示',
            content: '确认要删除吗？',
            showCancel: true,
            cancelText: '取消',
            confirmText: '确认',
            success: res => {
                if (res.confirm) {
                    ajaxPost('member/del_address', data, res => {
                        console.log(res)
                        if(res.code == 40020){
                            return showToast('text',res.msg)
                        }else if (res.code == 0) {
                            showToast("text", "删除成功")
                            addressList.splice(index, 1)

                            this.setData({
                                addressList: addressList
                            })
                            // if (addressList.length == 1) {
                            //     let data = {
                            //         id: this.data.addressList[0].id,
                            //         is_default: 1
                            //     }
                            //     ajaxPost("member/is_default", data, res => {
                            //         if (res.code == 0) {
                            //             showToast('text', res.msg)
                            //             ajaxPost('member/address', {}, res => {
                            //                 console.log(res)
                            //                 if (res.code == 0) {
                            //                     var data = res.data.dataList;
                            //                     this.setData({
                            //                         addressList: data
                            //                     })
                            //                 }
                            //             })

                            //         } else {
                            //             showToast('text', res.msg)
                            //         }
                            //     })
                            // }
                        } else {
                            showToast("text", res.msg)
                        }
                    })
                }
            }
        })

    },
       /**
     * 选择地址
     * @param {*} e 
     */
    chooseAddress(e) {
        var index = e.currentTarget.dataset.index;
        if (this.data.ischoose == false) {
            return
        } else {
            var pages = getCurrentPages(),
                currPage = pages[pages.length - 1],
                prevPage = pages[pages.length - 2];
            prevPage.setData({
                'exchangeDetail.member_address.addressid': this.data.addressList[index].id,
                'exchangeDetail.member_address.receiver': this.data.addressList[index].consignee,
                'exchangeDetail.member_address.mobile': this.data.addressList[index].mobile,
                'exchangeDetail.member_address.location': this.data.addressList[index].location,
                'exchangeDetail.member_address.address': this.data.addressList[index].address,
                'exchangeDetail.member_address.id': this.data.addressList[index].id,
                'exchangeDetail.member_address.receiver': this.data.addressList[index].receiver
            })
            wx.navigateBack({

            })
        }
    }
})