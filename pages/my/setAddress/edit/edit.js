// pages/my/setAddress/edit/edit.js
import {
    ajaxPost,
    showToast
} from '../../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        region: ['', '', ''],
        isregion: true,
        address: '',
        mobile: '',
        is_default: true,
        location: '',
        addressList:[],
        is_defaults:false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        // if(options.id)
        this.setData({
            address_id: options.id
        })
        
        this.getAddressDetail(options.id)
        this.getAddressList();
        
    },

    
   /**
     * 选择省市区
     * @param {*} e 
     */
    bindRegionChange: function(e) {
        console.log('picker发送选择改变，携带值为', e.detail.value)
        var region = e.detail.value[0] + e.detail.value[1] + e.detail.value[2]
        this.setData({
            region: e.detail.value,
            location: region,
            isregion: false
        })
    },
    //提交表单
    formSubmit(e) {
        var receiver = e.detail.value.receiver;
        var mobile = e.detail.value.mobile;
        var location = this.data.location;
        var address = e.detail.value.address;
        var is_default = e.detail.value.is_default;
        var myreg = /^[1][0-9]{10}$/;

        var addressList = this.data.addressList;
        if (receiver.length == 0) {
            return showToast('text', "请输入收货人")
        }
        if (!myreg.test(mobile)) {
            return showToast("text", "请输入正确手机号")
        }
        if (this.data.region[0].length == 0) {
            return showToast('text', "请选择地区")
        }
        if (address.length == 0) {
            return showToast('text', "请输入详细地址")
        }
        if (is_default) {
            is_default = 1
        } else {
            is_default = 0
        }
        

        if (this.data.address_id == undefined) {
            let data = {
                receiver: receiver,
                mobile: mobile,
                location: location,
                address: address,
                is_default: is_default,
                province: this.data.region[0],
                city: this.data.region[1],
                area: this.data.region[2]
            }
            ajaxPost('member/add_address', data, res => {
                console.log(res)
                if (res.code == 0) {
                    showToast("text", res.msg)
                    setTimeout(() => {
                        wx.navigateBack({
                            delta: 1,
                        })
                    }, 2000)

                } else {
                    showToast('text', res.msg)
                }
            })
            return
        } else {
            let data = {
                id: this.data.address_id,
                receiver: receiver,
                mobile: mobile,
                location: location,
                address: address,
                is_default: is_default,
                province: this.data.region[0],
                city: this.data.region[1],
                area: this.data.region[2]
            }
            ajaxPost('member/save_address', data, res => {
                console.log(res)
                if (res.code == 0) {
                    showToast("text", res.msg)
                    setTimeout(() => {
                        wx.navigateBack({
                            delta: 1,
                        })
                    }, 2000)

                } else {
                    showToast('text', res.msg)
                }
            })
        }
        console.log(this.data.address_id)
    },
       /**
     * 获取地址详情
     * @param {*} e 
     */
    getAddressDetail(id) {
        if (id == undefined) {
            return
        }
        let data = {
            id: id
        }
        ajaxPost('member/edit_address', data, res => {
            var data = res.data;
            var is_default = data.is_default;
            if (is_default == 0) {
                is_default = false
            } else {
                is_default = true
            }
            this.setData({
                receiver: data.receiver,
                mobile: data.mobile,
                region: [data.province, data.city, data.area],
                address: data.address,
                is_default: is_default,
                isregion: false,
                location: data.province + data.city + data.area
            })
            console.log(res)
        })
    },
       /**
     * 获取地址列表
     * @param {*} e 
     */
    getAddressList() {
        ajaxPost('member/address', {}, res => {
            console.log(res)
            var data = res.data.dataList;
            this.setData({
                addressList: data
            })
            if (data.length == 1) {
                if (this.options.add){
                    console.log('')
                }else{
                    this.setData({
                        is_defaults: true
                    })
                }
                
            }
        })
    },
})