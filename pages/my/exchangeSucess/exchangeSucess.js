// pages/my/exchangeSucess/exchangeSucess.js
Page({

    /**
     * 页面的初始数据
     */
    data: {

    },




    /**
     * 回到积分商城
     */

    goMall(){
        
        wx.switchTab({
            url: '/pages/scoresMall/index/index'
        })
    },
       /**
     * 跳转订单详情
     * @param {*} e 
     */
    navOrderDetail(){
        wx.navigateTo({
            url: '/pages/my/exchangeOrder/exchangeOrder'
        })
        
    }
})