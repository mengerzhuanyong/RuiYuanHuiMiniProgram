// pages/my/myCollect/myCollect.js
var app = getApp();
import {ajaxPost,showToast} from '../../../utils/util.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        collectList:[],
        limit:10,
        page:0,
        noGet:false
    },




    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.setData({
            collectList: []
        })
        var collectList = this.data.collectList;
        
        let data = {
            limit: this.data.limit,
            page: 1
        }
        ajaxPost('member/my_collect', data, res => {
            console.log(res)

            if (res.code == 0) {
                var data = res.data.dataList;
                if (data.length == 0) {
                    this.setData({
                        noGet: true,
                        page:1
                    })
                    wx.hideLoading()
                    return
                }
                else {
                    let arr = collectList.concat(data)
                    this.setData({
                        collectList: arr,
                        
                        page: 1
                    })
                }
                wx.hideLoading()
            }

        })
    },


    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        wx.showLoading({
            title: '加载中'
        })
        this.getCollectList()
    },
   /**
     * 获取收藏列表
     * @param {*} e 
     */
    getCollectList(){
        var collectList = this.data.collectList;
        if(this.data.noGet){
            wx.hideLoading()
            return
        }
        let data = {
            limit:this.data.limit,
            page:++this.data.page
        }
        ajaxPost('member/my_collect',data,res=>{
            console.log(res)
           
            if(res.code == 0){
                var data = res.data.dataList;
                if (data.length == 0) {
                    this.setData({
                        noGet: true
                    })
                    wx.hideLoading()
                    return
                }
                else {
                    let arr = collectList.concat(data)
                    this.setData({
                        collectList: arr
                    })
                }
                wx.hideLoading()
            }
            
        })
    },
       /**
     * 取消收藏
     * @param {*} e 
     */
    cancelCollect(e){
        var collectList = this.data.collectList;
        var goodid = e.currentTarget.dataset.goodid;
        var index = e.currentTarget.dataset.index;
        let data = {
            goods_id:goodid
        }
        ajaxPost('member/collect',data,res=>{
            if(res.code == 0){
                showToast('text',"取消成功")
                collectList.splice(index,1)
                this.setData({
                    collectList: collectList
                })
            }else{
                showToast('text',res.msg)
            }
        })
    },
       /**
     * 跳转商品详情
     * @param {*} e 
     */
    navGoodDetail(e) {

        wx.navigateTo({
            url: '/pages/scoresMall/good_detail/good_detail?id=' + e.currentTarget.dataset.id
        });

    },
       /**
     * 跳转兑换
     * @param {*} e 
     */
    navExchange(e) {
        var token = wx.getStorageSync("token");
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        } else {
            console.log(e)
            wx.navigateTo({
                url: '/pages/index/scoreExchange/scoreExchange?id=' + e.currentTarget.dataset.id
            })
        }

    }
})