// pages/my/exchangeDetail/exchangeDetail.js
import {ajaxPost,showToast} from '../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        exchangeDetail:{},
        id:0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            id: options.id
        })
        this.getExchangeDetail(options.id)
    },

 

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.getExchangeDetail(this.data.id)
    },



    /**
     * 跳转评价
     */

    navAppraise(){
        var id = this.data.id;
        wx.navigateTo({
            url: "/pages/index/appraise/appraise?id="+id
        })
    },
       /**
     * 查看评价
     * @param {*} e 
     */
    lookAppraise(e) {
        wx.navigateTo({
            url: '/pages/index/lookappraise/lookappraise?id=' + e.currentTarget.dataset.id + '&evaluateid=' + e.currentTarget.dataset.evaluateid
        })
    },
   /**
     * 获取兑换详情
     * @param {*} e 
     */
    getExchangeDetail(id){
        
        ajaxPost('member/goods_detail',{id:id},res=>{
            console.log(res)
            var data = res.data;
            this.setData({
                exchangeDetail:data
            })
        })
    },
       /**
     * 复制id
     * @param {*} e 
     */
    copyId(e){
        var payname = e.currentTarget.dataset.payname;
        wx.setClipboardData({
            data:payname,
            fail:res=>{
                showToast('text',"复制失败")
            }
        })
    },
       /**
     * 确认收货
     * @param {*} e 
     */
    confirmGetgood() {
        var id = this.data.id;
        let data = {
            goods_pay_id: id
        }
        wx.showModal({
            title: '提示',
            content: '确定要收货吗？',
            showCancel: '取消',
            success: res => {
                if (res.confirm) {
                    ajaxPost('member/confirm_receipt', data, res => {
                        if (res.code == 0) {
                            showToast('text', res.msg)
                            this.getExchangeDetail(id)
                        }
                    })
                }
            }
        })

    },
    //退款
    refound(e) {
        var id = e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '/pages/refound/refound?id=' + id
        })
    },
    /**
 * 跳转商品详情
 * @param {*} e 
 */
    navGoodDetail(e) {
        wx.navigateTo({
            url: '/pages/scoresMall/good_detail/good_detail?id=' + e.currentTarget.dataset.id,
        })
    },
    /**
 * 跳转积分兑换页面
 *  {*} e 
 */
    navExchange(e) {
        var token = wx.getStorageSync("token");
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        } else {
            console.log(e)
            wx.navigateTo({
                url: '/pages/index/scoreExchange/scoreExchange?id=' + e.currentTarget.dataset.id
            })
        }

    },
})