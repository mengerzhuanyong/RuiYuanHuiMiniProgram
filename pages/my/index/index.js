// pages/my/my.js
var app = getApp()
import {
    ajaxPost,
    showToast
} from '../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        navList: [{
            icon: 'https://ruiyuanhui.3todo.com/uploads/img/invert_friend.png',
            text: "邀请好友",
            url: "/pages/my/inviteFriend/inviteFriend"
        }, {
            icon: "https://ruiyuanhui.3todo.com/uploads/img/checkin-points.png",
            text: "签到赢积分",
            url: "/pages/scoresMall/getScores/getScores"
        }, {
            icon: "https://ruiyuanhui.3todo.com/uploads/img/task.png",
            text: "任务领积分",
            url: "/pages/my/taskScore/taskScore"
        }, {
            icon: "https://ruiyuanhui.3todo.com/uploads/img/paymoney.png",
            text: "消费兑换",
            url: "/pages/my/certScore/certScore"
        }],
        isLogin: true,
        userInfo: {},
        jinyong:true,
        goodsList:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        // this.getIndexData()
        console.log(this.data.userInfo.length)
    },



    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        var token = wx.getStorageSync("token")
        if (!token) {

            return
        }
        this.setData({
            isLogin:true
        })
        ajaxPost('member/index', {}, res => {
            if(res.code == 60000){
                wx.removeStorageSync('token')
                return
            }
            else if (res.code == 40020){
                this.setData({
                    jinyong: false
                })
                return showToast('text', res.msg)
               
                  
                
            }else if(res.code == 0){
                var data = res.data.member;
                var recommend_goods = res.data.recommend_goods;
                var top_banner = res.data.top_banner;
                this.setData({
                    jinyong: true,
                    userInfo: data,
                    goodsList: recommend_goods,
                    top_banner: top_banner
                })
            }
            
        })
    },



    /**
     * 跳转tab
     */

    navTab(e) {
        var token = wx.getStorageSync("token")
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        }
        wx.navigateTo({
            url: e.currentTarget.dataset.url
        })
    },
       /**
     * 获取页面数据
     * @param {*} e 
     */
    getIndexData() {
        ajaxPost('index/index', {}, res => {
            var data = res.data
            this.setData({
                bannerUrls: data.album,
                newsList: data.news,
                goodsList: data.goods
            })
        })
    },
    //跳转到会员卡页面
    navMemberCard(){
        var token = wx.getStorageSync("token")
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        }
        wx.navigateTo({
            url: '/pages/my/memberCard/memberCard'
        });
    },
       /**
     * 跳转商品详情
     * @param {*} e 
     */
    navGoodDetail(e) {

        wx.navigateTo({
            url: '/pages/scoresMall/good_detail/good_detail?id=' + e.currentTarget.dataset.id
        });

    },
       /**
     * 跳转兑换列表
     * @param {*} e 
     */
    navExchangeList() {
        var token = wx.getStorageSync("token")
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        }
        wx.navigateTo({
            url: '/pages/my/exchangeOrder/exchangeOrder'
        })
    },
       /**
     * 暂不登录
     * @param {*} e 
     */
    noLogin() {
        this.setData({
            isLogin: true
        })
    },
       /**
     * 跳转新闻详情
     * @param {*} e 
     */
    navNewsDetail() {
        wx.navigateTo({
            url: '/pages/index/news_detail/news_detail',
        })
    },
       /**
     * 查看个人信息
     * @param {*} e 
     */
    lookMember() {
        var token = wx.getStorageSync("token")
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        }
        wx.navigateTo({
            url: '/pages/my/memberRight/memberRight'
        })
    },
       /**
     * 跳转编辑信息
     * @param {*} e 
     */
    navMine() {
        var token = wx.getStorageSync("token")
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        }
        wx.navigateTo({
            url: '/pages/my/edit_index/edit_index'
        })
    },
       /**
     * 跳转余额
     * @param {*} e 
     */
    navMoney() {
        var token = wx.getStorageSync("token")
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        }
        wx.navigateTo({
            url: '/pages/my/myMoney/myMoney'
        })
    },
       /**
     * 跳转收藏
     * @param {*} e 
     */
    navCollect() {
        var token = wx.getStorageSync("token")
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        }
        wx.navigateTo({
            url: '/pages/my/myCollect/myCollect'
        })
    },
       /**
     * 跳转积分
     * @param {*} e 
     */
    navMyScore() {
        var token = wx.getStorageSync("token")
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        }
        wx.navigateTo({
            url: '/pages/scoresMall/myScores/myScores'
        })
    },
    
   /**
     * 跳转兑换页面
     * @param {*} e 
     */
    navExchange(e) {
        var token = wx.getStorageSync("token");
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        } else {
            console.log(e)
            wx.navigateTo({
                url: '/pages/index/scoreExchange/scoreExchange?id=' + e.currentTarget.dataset.id
            })
        }

    },
       /**
     * 跳转h5
     * @param {*} e 
     */
    navh5(e) {
        var url = e.currentTarget.dataset.url;
        if (url.indexOf('/pages/') != -1){
            wx.navigateTo({
                url: url
            })
        }else{
            wx.navigateTo({
                url: '/pages/h5/h5?url=' + url
            })
        }
    },
       /**
     * 跳转引导页
     * @param {*} e 
     */
    navGuide() {
        wx.navigateTo({
            url: '/pages/guide/guide'
        })
    },
})