// pages/my/certScore/certScore.js
var app = getApp()
import { qiniuUpload, ajaxPost, showToast } from '../../../utils/util.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        imgUrl: [],
        record:[],
        limit:10,
        page:0,
        noGet:false,
        imgs:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getOcrList()
    },

   

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        wx.showLoading({
            title: '加载中',
        })
        this.getOcrList()
    },


    /**
*  上传单张图片公共方法
* @param {*} e  穿参
*/
    businessUpload() {
        var arr = []
        var that = this;
        wx.chooseImage({
            count: 9,
            sizeType: ['original', 'compressed'],
            sourceType: ['album', 'camera'],
            success(res) {
                qiniuUpload.upload(res.tempFilePaths, res => {
                    console.log(res)
                    let imgUrl = that.data.imgUrl
                    for (let i = 0; i < res.length; i++) {
                        imgUrl.push(app.globalData.QINIU_IMG_URL + res[i].key)
                    }
                    that.setData({
                        imgUrl: imgUrl,
                        imgs:imgUrl.join('|')
                    })
                })
            }
        })
    },
       /**
     * 提交表单
     * @param {*} e 
     */
    bindSubmit(e) {
        var imgUrl = this.data.imgUrl;
        var imgs = this.data.imgs;
        if (imgUrl.length == 0) {
            return showToast('text', '请提交图片')
        }
        let data = {
            image: imgs,
        }
        ajaxPost('ocr/index', data, res => {
            if (res.code == 0) {
                showToast('text', '上传成功，等待审核~')
                this.setData({
                    imgUrl: []
                })
                ajaxPost('ocr/ocr_list', data, res => {
                    if (res.code == 0) {
                        var data = res.data.dataList;
                            this.setData({
                                record: data
                            })
                        }
                    
                })
            }
        })
    },
       /**
     * 获取提交的数据
     * @param {*} e 
     */
    getOcrList(){
        if(this.data.noGet){
            wx.hideLoading()
            return
        }
        let data = {
            limit:this.data.limit,
            page:++this.data.page
        }
        var record = this.data.record;
        ajaxPost('ocr/ocr_list',data,res=>{
            console.log(res)
            
            if (res.code == 0) {
                var data = res.data.dataList;
                if (data.length == 0) {
                    this.setData({
                        noGet: true
                    })
                    wx.hideLoading()
                    return
                }
                else {
                    let arr = record.concat(data)
                    this.setData({
                        record: arr
                    })
                }
                wx.hideLoading()
            }
        })
    },
       /**
     * 删除图片
     * @param {*} e 
     */
    delImg(e) {
        var imgUrl = this.data.imgUrl;
        var index = e.currentTarget.dataset.index;
        imgUrl.splice(index, 1);
        this.setData({
            imgUrl: imgUrl, 
            imgs: imgUrl.join('|')
        })
    }
})