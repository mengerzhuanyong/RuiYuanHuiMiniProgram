// pages/my/edit_index/edit_index.js
var app = getApp()
import {qiniuUpload,ajaxPost,showToast} from '../../../utils/util.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        memberInfo:{}
    },


    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.getMemberInfo()
    },


    /**
     * 跳转意见反馈页面
     */

    navFeedback(){
        wx.navigateTo({
            url: '/pages/my/feedback/feedback'
        })
    },
       /**
     * 跳转地址管理页面
     * @param {*} e 
     */
    navAddress(){
        wx.navigateTo({
            url: '/pages/my/setAddress/index/index'
        })
    },
       /**
     * 跳转变更电话页面
     * @param {*} e 
     */
    navChangeTel(){
        wx.navigateTo({
            url: '/pages/my/changeTel/changeTel'
        })
    },
       /**
     * 跳转绑定电话页面
     * @param {*} e 
     */
    navBindTel(){
        wx.navigateTo({
            url: '/pages/my/bindTel/bindTel',
        })
    },
       /**
     * 获取用户信息
     * @param {*} e 
     */
    getMemberInfo(){
        ajaxPost('member/index',{},res=>{
            console.log(res)
            var data = res.data;
            this.setData({
                memberInfo:data.member
            })
        })
    },
       /**
     * 跳转编辑昵称页面
     * @param {*} e 
     */
    navEditName(e){
        wx.navigateTo({
            url: '/pages/my/editName/editName'
        })
    },
    /**
*  上传单张图片公共方法
* @param {*} e  穿参
*/
    businessUpload() {
        var arr = []
        var that = this;
        wx.chooseImage({
            count: 1,
            sizeType: ['original', 'compressed'],
            sourceType: ['album', 'camera'],
            success(res) {
                qiniuUpload.upload(res.tempFilePaths, res => {
                    console.log(res)
                    let data = {
                        head_portrait:app.globalData.QINIU_IMG_URL + res[0].key
                    }
                    ajaxPost('member/save_avatar',data,res=>{
                        if(res.code == 0){
                            that.getMemberInfo()
                        }
                    })
                })
            }
        })
    },
    // 打电话
    callPhone(e){
        var num = e.currentTarget.dataset.num;
        wx.makePhoneCall({
            phoneNumber: num
        })
    }
})