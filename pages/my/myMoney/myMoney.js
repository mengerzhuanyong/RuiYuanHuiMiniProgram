// pages/my/myMoney/myMoney.js
import { showToast, ajaxPost } from '../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        moneyList: {},
        doGet: false,
        page: 0,
        limit: 10,
        chooseTime: ['最近一个月', '最近三个月','最近半年'],
        index:0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        if (this.data.noGet) {
            wx.hideLoading()
            return;
        }
        let data = {
            page: ++this.data.page,
            limit: this.data.limit,
            moon:this.data.index + 1
        };
        ajaxPost('member/balance_log', data, res => {
            console.log(res)
            var data = res.data;
            if (res.code == 0) {
                this.setData({
                    moneyList: data
                })
            }
        })
    },
    onShow(){
        if (this.data.noGet) {
            wx.hideLoading()
            return;
        }
        let data = {
            page: this.data.page,
            limit: this.data.limit,
            moon: this.data.index + 1
        };
        ajaxPost('member/balance_log', data, res => {
            console.log(res)
            var data = res.data;
            if (res.code == 0) {
                this.setData({
                    moneyList: data
                })
            }
        })
    },


    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        wx.showLoading({
            title: '加载中',
        })
        this.getMoneyList()
    },
   /**
     * 跳转充值
     * @param {*} e 
     */
    recharge(){
        wx.navigateTo({
            url: '/pages/my/investMoney/investMoney'
        })
    },
       /**
     * 获取余额信息
     * @param {*} e 
     */
    getMoneyList() {
        var moneyList = this.data.moneyList;
        if (this.data.noGet) {
            wx.hideLoading()
            return;
        }
        let data = {
            page: ++this.data.page,
            limit: this.data.limit,
            moon: this.data.index + 1
        };
        ajaxPost('member/balance_log', data, res => {
            console.log(res)
            var data = res.data.list
            if (res.code == 0) {
                if (data.length == 0) {
                    this.setData({
                        noGet: true
                    })
                } else {
                    var data = res.data;
                    let newarr = moneyList.list.concat(data.list)
                    this.setData({
                        'moneyList.list': newarr
                    })
                }
                wx.hideLoading()

            }
        })
    },
    bindPickerChange: function (e) {
        console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
            index: Number(e.detail.value)
        })
        var moneyList = this.data.moneyList;
        
        let data = {
            page: 1,
            limit: this.data.limit,
            moon: this.data.index + 1
        };
        ajaxPost('member/balance_log', data, res => {
            console.log(res)
            this.setData({
                'moneyList.list':[]
            })
            if (res.code == 0) {
                var data = res.data.list
                
                    this.setData({
                        'moneyList.list': data,
                        noGet:false
                    })
                
                wx.hideLoading()

            }
        })
    },
})