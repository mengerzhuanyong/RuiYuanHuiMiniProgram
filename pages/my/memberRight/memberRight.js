// pages/my/memberRight/memberRight.js
var app = getApp()
import {ajaxPost,showToast} from '../../../utils/util.js'
var WxParse = require('../../../wxParse/wxParse.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        memberInfo:{}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getMemberInfo()
    },

   /**
     * 获取会员信息及规则
     * @param {*} e 
     */
    getMemberInfo(){
        ajaxPost('rule/index',{},res=>{
            if(res.code == 0){
                var data = res.data;
                this.setData({
                    memberInfo:data
                })
                var article = data.rule_content;
                /**
                 * WxParse.wxParse(bindName , type, data, target,imagePadding)
                 * 1.bindName绑定的数据名(必填)
                 * 2.type可以为html或者md(必填)
                 * 3.data为传入的具体数据(必填)
                 * 4.target为Page对象,一般为this(必填)
                 * 5.imagePadding为当图片自适应是左右的单一padding(默认为0,可选)
                 */

                WxParse.wxParse('article', 'html', article, this, 5);
            }
        })
    }
})