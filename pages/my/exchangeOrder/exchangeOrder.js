// pages/my/exchangeRecord/exchangeRecord.js
import {
    ajaxPost,
    showToast
} from '../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        navList: ['全部', '待发货', '待收货', '已收货', '退款'],
        currentIndex: 0,
        status: 0,
        exchangeList: [],
        noGet: false,
        page: 0,
        limit: 10
    },



    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.getExchangeOrder(this.data.currentIndex)
    },



    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        wx.showLoading({
            title: '加载中',
        })
        this.getExchangeOrder()
    },

    //切换Tab
    navChange(e) {
        this.getExchangeOrder(e.currentTarget.dataset.index)


    },
    /**
  * 跳转兑换详情
  * @param {*} e 
  */
    navDetail(e) {
        wx.navigateTo({
            url: '/pages/my/exchangeDetail/exchangeDetail?id=' + e.currentTarget.dataset.id
        })
    },
    /**
  * 查看评价
  * @param {*} e 
  */
    lookAppraise(e) {
        wx.navigateTo({
            url: '/pages/index/lookappraise/lookappraise?id=' + e.currentTarget.dataset.id + '&evaluateid=' + e.currentTarget.dataset.evaluateid
        })
    },
    /**
  * 获取兑换列表
  * @param {*} e 
  */
    getExchangeOrder(index) {
        wx.showLoading({
            title: '加载中',
            mask: true
        })
        if (currentIndex !== index) {
            this.setData({
                noGet: false,
                exchangeList: [],
                page: 0,
                currentIndex: index
            })
        }

        var currentIndex = this.data.currentIndex;
        var exchangeList = this.data.exchangeList;

        if (this.data.noGet) {
            wx.hideLoading()
            return;
        }


        let data = {
            page: ++this.data.page,
            limit: this.data.limit,
            is_type: currentIndex
        }
        if (currentIndex == 0) {
            data = {
                page: this.data.page,
                limit: this.data.limit,
                is_type: ''
            }
        }
        else if (currentIndex == 4) {
            data = {
                page: this.data.page,
                limit: this.data.limit,
                is_type: 5
            }
        }
        else {
            data = {
                page: this.data.page,
                limit: this.data.limit,
                is_type: currentIndex - 1
            }
        }
        ajaxPost('member/goods_record', data, res => {
            if (res.code == 0) {
                var data = res.data.dataList;
                if (data.length == 0) {
                    this.setData({
                        noGet: true
                    })
                    wx.hideLoading()
                    return
                } else {

                    let newarr = exchangeList.concat(data)
                    this.setData({
                        exchangeList: newarr
                    })
                }
                wx.hideLoading()
            }


        })
    },
    /**
  * 确认收货
  * @param {*} e 
  */
    confirmGetgood(e) {
        var id = e.currentTarget.dataset.id;
        let data = {
            goods_pay_id: id
        }
        wx.showModal({
            title: '提示',
            content: '确定要收货吗？',
            showCancel: '取消',
            success: res => {
                if (res.confirm) {
                    ajaxPost('member/confirm_receipt', data, res => {
                        if (res.code == 0) {
                            showToast('text', res.msg)
                            wx.showLoading({
                                title: '加载中',
                            })
                            let data = {
                                is_type: this.data.currentIndex - 1
                            }
                            if (this.data.currentIndex == 0) {
                                data = {
                                    is_type: ''
                                }
                            }


                            ajaxPost('member/goods_record', data, res => {
                                if (res.code == 0) {
                                    var data = res.data.dataList;


                                    this.setData({
                                        exchangeList: data
                                    })

                                    wx.hideLoading()
                                }


                            })
                        }
                    })
                }
            }
        })

    },
    //退款
    refound(e) {
        var id = e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '/pages/refound/refound?id=' + id
        })
    },
    /**
  * 跳转评价
  * @param {*} e 
  */
    navAppraise(e) {
        wx.navigateTo({
            url: "/pages/index/appraise/appraise?id=" + e.currentTarget.dataset.id
        })
    },
})