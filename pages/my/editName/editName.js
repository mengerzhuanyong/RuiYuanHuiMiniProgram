// pages/my/editName/editName.js
import {ajaxPost,showToast} from '../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        inputValue:'',
        nickname:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getMemberInfo()
    },

 
   /**
     * 获取用户信息
     * @param {*} e 
     */
    getMemberInfo() {
        ajaxPost('member/index', {}, res => {
            console.log(res)
            var data = res.data;
            this.setData({
                memberInfo: data.member,
                nickname: data.member.nickname
            })
        })
    },
       /**
     * 绑定输入
     * @param {*} e 
     */
    bindInput(e){
        this.setData({
            inputValue:e.detail.value
        })
    },
       /**
     * 提交输入的昵称
     * @param {*} e 
     */
    changeValue(){
        
        let data = {
            name: this.data.inputValue
        }
        if (this.data.inputValue == '') {
            data = {
                name: this.data.nickname
            }
        }
        ajaxPost('member/edit_info',data,res=>{
            if(res.code == 0){
                showToast('text',"修改成功")
                setTimeout(()=>{
                    wx.navigateBack({
                        delta: 1,
                    })
                }, 2000)
            }else{
                showToast('text', res.msg)
            }
        })
    }
})