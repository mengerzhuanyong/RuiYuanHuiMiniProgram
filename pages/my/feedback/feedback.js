// pages/my/feedback/feedback.js
var app = getApp()
import { qiniuUpload,ajaxPost,showToast} from '../../../utils/util.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        imgUrl:[],
        remark:''
    },



    /**
 *  上传单张图片公共方法
 * @param {*} e  穿参
 */
    businessUpload() {
        var arr = []
        var that = this;
        wx.chooseImage({
            count: 9,
            sizeType: ['original', 'compressed'],
            sourceType: ['album', 'camera'],
            success(res) {
                qiniuUpload.upload(res.tempFilePaths, res => {
                    console.log(res)
                    let imgUrl = that.data.imgUrl
                    for (let i = 0; i < res.length; i++) {
                        imgUrl.push(app.globalData.QINIU_IMG_URL + res[i].key)
                    }
                    that.setData({
                        imgUrl: imgUrl
                    })
                })
            }
        })
    },
       /**
     * 提交反馈
     * @param {*} e 
     */
    bindSubmit(e){
        
        var remark = e.detail.value.remark;
        var imgUrl = this.data.imgUrl;
        if (remark.length == 0 && imgUrl.length == 0){
            return showToast('text','请输入内容或提交图片')
        }
        let data = {
            images:imgUrl,
            remark:remark
        }
        ajaxPost('member/suggest',data,res=>{
            if(res.code == 0){
                showToast('text', res.msg)
                this.setData({
                    remark:'',
                    imgUrl:[]
                })
            }
        })
    },
       /**
     * 删除图片
     * @param {*} e 
     */
    delImg(e){
        var imgUrl = this.data.imgUrl;
        var index = e.currentTarget.dataset.index;
        imgUrl.splice(index,1);
        this.setData({
            imgUrl:imgUrl
        })
    }
})