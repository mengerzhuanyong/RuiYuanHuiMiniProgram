//Page Object
import {
    ajaxPost,
    showToast,
    formatTime
} from '../../../utils/util.js';
var app = getApp()
Page({
    data: {
        index: 0,
        page: 0,
        limit: 10,
        doGet: false,
        goodsList: [],
        currenttab: 0,
        isLogin: true,
        timestamp: 0,
        actEndTimeList:[]
    },
    //options(Object)
    onShow(){
        var tmp = Date.parse(new Date()).toString();
        tmp = tmp.substr(0, 10);
        this.setData({
            timestamp: Number(tmp)
        })
    },
    onLoad: function(options) {

        this.setData({
            index: options.index
        })
        this.getIndexList(options.index)
    },
       /**
     * 底部上拉加载
     * @param {*} e 
     */
    onReachBottom: function() {
        var index = this.data.index;
        wx.showLoading({
            title: '加载中',
        })
        this.getIndexList(index)
    },
       /**
     * 获取商品列表
     * @param {*} e 
     */
    getIndexList(index) {
        let endTimeList  = [];
        var goodsList = this.data.goodsList;
        if (index == 1) {
            if (this.data.doGet) {
                wx.hideLoading()
                return
            }
            let data = {
                limit: this.data.limit,
                page: ++this.data.page
            }
            ajaxPost('goods/more_hot_goods', data, res => {
                if (res.code == 0) {
                    var data = res.data.list;
                    wx.setNavigationBarTitle({
                        title: '推荐商品列表'
                    })
                    if (data.length == 0) {
                        wx.hideLoading()
                        this.setData({
                            doGet: true
                        })
                        return
                    }


                    let arr = goodsList.concat(data);
                    this.setData({
                        goodsList: arr
                    })
                    wx.hideLoading()
                }
            })
        } else if (index == 2) {
            if (this.data.doGet) {
                wx.hideLoading()
                return
            }
            let data = {
                limit: this.data.limit,
                page: ++this.data.page
            }
            ajaxPost('goods/more_time_goods', data, res => {
                if (res.code == 0) {
                    var data = res.data.list;
                    wx.setNavigationBarTitle({
                        title: '限时商品列表'
                    })
                    if (data.length == 0) {
                        wx.hideLoading()
                        this.setData({
                            doGet:true
                        })
                        return
                    }


                    let arr = goodsList.concat(data);
                    console.log(arr)
                    for(var i = 0;i<arr.length;i++){
                        goodsList[i].daojishi = formatTime(new Date(arr[i].end_time*1000))
                        
                    }
                    arr.forEach(o=>{
                        endTimeList.push(o.daojishi)
                    })
                    this.setData({
                        goodsList: arr,
                        actEndTimeList: endTimeList
                    })
                    this.countDown()
                    wx.hideLoading()
                }
            })
        }
        else if (index == 3) {
            if (this.data.doGet) {
                wx.hideLoading()
                return
            }
            let data = {
                limit: this.data.limit,
                page: ++this.data.page
            }
            ajaxPost('goods/more_hot_good', data, res => {
                if (res.code == 0) {
                    var data = res.data.list;
                    wx.setNavigationBarTitle({
                        title: '推荐商品列表'
                    })
                    if (data.length == 0) {
                        wx.hideLoading()
                        this.setData({
                            doGet: true
                        })
                        return
                    }


                    let arr = goodsList.concat(data);
                    this.setData({
                        goodsList: arr
                    })
                    wx.hideLoading()
                }
            })
        }
        else if (index == 4) {
            if (this.data.doGet) {
                wx.hideLoading()
                return
            }
            let data = {
                limit: this.data.limit,
                page: ++this.data.page
            }
            ajaxPost('goods/more_time_good', data, res => {
                if (res.code == 0) {
                    var data = res.data.list;
                    wx.setNavigationBarTitle({
                        title: '限时商品列表'
                    })
                    if (data.length == 0) {
                        wx.hideLoading()
                        this.setData({
                            doGet: true
                        })
                        return
                    }


                    let arr = goodsList.concat(data);
                    for(var i = 0;i<arr.length;i++){
                        arr[i].daojishi = formatTime(new Date(arr[i].end_time*1000))
                        
                    }
                    arr.forEach(o=>{
                        endTimeList.push(o.daojishi)
                    })
                    this.setData({
                        goodsList: arr,
                        actEndTimeList: endTimeList
                    })
                    this.countDown()
                    wx.hideLoading()
                }
            })
        }
    },
    timeFormat(param) { //小于10的格式化函数
        return param < 10 ? '0' + param : param;
    },
    countDown() { //倒计时函数
        // 获取当前时间，同时得到活动结束时间数组
        let newTime = new Date().getTime();
        let actEndTimeList = this.data.actEndTimeList;
        let countDownArr = [];
        let timeGoods = this.data.goodsList;

        // 对结束时间进行处理渲染到页面
        actEndTimeList.forEach(o => {
            let endTime = new Date(o).getTime();
            
            let obj = null;
            // 如果活动未结束，对时间进行处理
            if (endTime - newTime > 0) {
                let time = (endTime - newTime);
                time = time / 1000;
                // 获取天、时、分、秒
                let day = parseInt(time / (60 * 60 * 24));
                let hou = parseInt(time % (60 * 60 * 24) / 3600);
                let min = parseInt(time % (60 * 60 * 24) % 3600 / 60);
                let sec = parseInt(time % (60 * 60 * 24) % 3600 % 60);
                obj = {
                    day: this.timeFormat(day),
                    hou: this.timeFormat(hou),
                    min: this.timeFormat(min),
                    sec: this.timeFormat(sec)
                }
                
            } else { //活动已结束，全部设置为'00'
                // obj = {
                //     day: '00',
                //     hou: '00',
                //     min: '00',
                //     sec: '00'
                // }

                this.getDataList()
            }
            countDownArr.push(obj)
            // timeGoods.forEach(o=>{
            //     o.date = obj
            // })
            // countDownArr.push(obj);
            for(let i = 0;i<timeGoods.length;i++){
                timeGoods[i].date = countDownArr[i]
            }
            // timeGoods.push(obj)
        })
        // 渲染，然后每隔一秒执行一次倒计时函数
        this.setData({
            goodsList: timeGoods
        })
        setTimeout(this.countDown, 1000);
    },
       /**
     * 跳转商品详情
     * @param {*} e 
     */
    navGoodDetail(e) {

        wx.navigateTo({
            url: '/pages/scoresMall/good_detail/good_detail?id=' + e.currentTarget.dataset.id
        });

    },
       /**
     * 跳转积分兑换页面
     * @param {*} e 
     */
    navExchange(e) {
        var token = wx.getStorageSync("token");
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        } else {
            console.log(e)
            wx.navigateTo({
                url: '/pages/index/scoreExchange/scoreExchange?id=' + e.currentTarget.dataset.id
            })
        }

    },
       /**
     * 暂不登录
     * @param {*} e 
     */
    noLogin() {
        this.setData({
            isLogin: true
        })
    },
       /**
     * 跳转引导页
     * @param {*} e 
     */
    navGuide() {
        wx.navigateTo({
            url: '/pages/guide/guide'
        })
    },
});