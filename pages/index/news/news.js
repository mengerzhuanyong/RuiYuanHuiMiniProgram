// pages/index/news/news.js
var app = getApp();
import {ajaxPost,showToast} from '../../../utils/util';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        tabList: ['瑞源资讯','业务资讯'],
        tabCurrent:0,
        limit:10,
        page:0,
        noGet:false,
        newsList:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        if(options.id !== undefined){
            this.getNewsList(Number(options.id)+1)
            return
        }
        this.getNewsList(this.data.tabCurrent+1)
    },

    

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        wx.showLoading({
            title: '加载中',
        })
        var id = this.data.tabCurrent + 1
        this.getNewsList(id)
    },

    /**
     * tab切换
     */
    tabChange(e){
        var index = e.currentTarget.dataset.index;
     
        var id = index + 1
        this.getNewsList(id)
    },
    /**
     * 获取新闻列表
     */
    getNewsList(id){
        if(this.data.tabCurrent !== id - 1){
            this.setData({
                newsList:[],
                page:0,
                noGet:false,
                tabCurrent: id - 1
            })
        }
       
        var newsList = this.data.newsList;
        if(this.data.noGet){
            wx.hideLoading()
            return
        }
        
        let data = {
            category_id: id,
            limit:this.data.limit,
            page:++this.data.page
        }
        ajaxPost('news/index', data,res=>{
            console.log(res)
            if(res.code == 0){
                var data = res.data.dataList
                
                if(data.length == 0){
                    this.setData({
                        noGet:true
                    })
                    wx.hideLoading()
                    return
                }else{
                    let arr = newsList.concat(data);
                    this.setData({
                        newsList:arr
                    })
                }
                wx.hideLoading()
            }
            
        })
    },
       /**
     * 跳转新闻详情
     * @param {*} e 
     */
    navNewsDetail(e){
        wx.navigateTo({
            url: '/pages/index/news_detail/news_detail?id=' + e.currentTarget.dataset.id
        })
    }
})