// pages/scoreExchange/scoreExchange.js
import {
    ajaxPost,
    showToast
} from '../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        hidepop: true,
        goodId: '',
        exchangeDetail: {},
        peisong: ['配送'],
        index: 0,
        agree_xieyi: false,
        mymoney: '',
        youfei: 0,
        isMail: 0,
        addressList: [],
        jinyong: true,
        num: 1
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        console.log(options)
        this.setData({
            goodId: options.id
        })

    },



    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        this.getExchangeDetail(this.options.id);
        ajaxPost('member/address', {}, res => {
            console.log(res)
            var data = res.data.dataList;
            this.setData({
                addressList: data
            })
        })
        ajaxPost('goods/exchange', {
            id: this.options.id
        }, res => {
            if (res.code == 0) {
                var data = res.data;
                this.setData({
                    'exchangeDetail.member_balance': Number(data.member_balance)
                })
            }
        })
    },



    /**
     * 跳转地址管理
     */
    navAddress() {
        wx.navigateTo({
            url: '/pages/my/setAddress/index/index?ischoose=true'
        })
    },
    confirmExchange(e) {
        var number = Number(e.currentTarget.dataset.number);
        var paynumber = Number(e.currentTarget.dataset.paynumber);
        var member_point = Number(this.data.exchangeDetail.member_point);
        var member_balance = Number(this.data.exchangeDetail.member_balance);
        var money = Number(e.currentTarget.dataset.money);
        var integral = Number(e.currentTarget.dataset.integral);
  
        var mymoney = Number(this.data.mymoney);
        var youfei = Number(this.data.youfei);
        var agree_xieyi = this.data.agree_xieyi;
        var num = Number(this.data.num);
        paynumber = num + paynumber;
        integral = integral * num;
        money = money * num;
        if (this.data.index == "0") {
            money = money  + youfei
        }
        if (agree_xieyi == false) {
            return showToast('text', "请阅读并勾选协议")
        }
        if (this.data.index == "0") {
            if (this.data.exchangeDetail.member_address == null) {
                return showToast('text', '请绑定收货地址')
            }
        }
        if (integral > member_point) {
            return showToast('text', '积分不足')
        }
        if (paynumber > number) {
            return showToast('text', '超过限购数量')
        }
        if (money > member_balance) {
            //还需要付的钱
            mymoney = money - member_balance;
            this.setData({
                hidepop: false,
                mymoney: mymoney
            })
            return
        }

        let data = {
            total_sum: money,
            goods_id: this.data.goodId,
            address_id: '',
            num: this.data.num,
            point: integral,
            is_type: 4
        }
        if (this.data.index == "0") {
            delete data.is_type
            data = {
                total_sum: money,
                goods_id: this.data.goodId,
                address_id: this.data.exchangeDetail.member_address.id,
                num: this.data.num,
                point: integral
            }
        }






        wx.showModal({
            title: '提示',
            content: '确认要兑换吗？',
            showCancel: true,
            cancelText: '取消',
            confirmText: '确认',
            success: res => {
                if (res.confirm) {
                    ajaxPost('goods/save_goods', data, res => {
                        if (res.code == 0) {
                            showToast('text', "兑换成功！")
                            setTimeout(() => {
                                wx.redirectTo({
                                    url: '/pages/my/exchangeSucess/exchangeSucess'
                                })
                            }, 2000)
                        } else if (res.code == 80001) {
                            this.setData({
                                hidepop: false,
                                mymoney: mymoney
                            })
                        } else if (res.code == 80003) {
                            showToast('text', res.msg)
                        } else {
                            showToast('text', res.msg)
                        }
                    })
                }
            },
        })


    },
    //关闭弹窗
    hidePop() {
        this.setData({
            hidepop: true
        })
    },
    //减数量
    jianNum() {
        var num = this.data.num;
        if (num <= 1) {
            this.setData({
                num: 1
            })
            return
        }
        this.setData({
            num: Number(num) - 1
        })
    },
    //加数量
    addNum() {
        var num = this.data.num;
        
        this.setData({
            num: Number(num) + 1
        })
    },

    //数量改变
    numChange(e){
        var num = this.data.num;
        if(e.detail.value <= 1){
            this.setData({
                num: 1
            })
            return
        }
        this.setData({
            num:Number(e.detail.value)
        })
    },
    /**
     * 获取兑换详情
     * @param {*} e 
     */
    getExchangeDetail(id) {
        var youfei = Number(this.data.youfei);
        var exchangeDetail = this.data.exchangeDetail;
        var addressList = this.data.addressList;
        let data = {
            id: id
        }
        ajaxPost('goods/exchange', data, res => {
            console.log(res)
            if (res.code == 40020) {
                this.setData({
                    jinyong: false
                })
                return showToast('text', res.msg)
            }
            this.setData({
                jinyong: true
            })
            var data = res.data;
            // if (this.data.exchangeDetail.member_address == null) {
            //     this.setData({
            //         exchangeDetail: data
            //     })
            //     return
            // }

            if (exchangeDetail.member_address == undefined) {
                data.goods.money = Number(data.goods.money)
                data = data

                if (data.goods.mail_money == null) {

                    this.setData({
                        exchangeDetail: data,
                        youfei: 0,
                        isMail: Number(data.goods.is_mail)
                    })
                    return
                }
                this.setData({
                    exchangeDetail: data,
                    youfei: Number(data.goods.mail_money),
                    isMail: Number(data.goods.is_mail)
                })
                return
            }

            if (this.data.addressList.length == 1) {
                exchangeDetail.member_address = this.data.addressList[0]
                exchangeDetail = exchangeDetail
                this.setData({
                    exchangeDetail: exchangeDetail
                })
            }

            // if (data.member_address == null) {
            //     this.setData({
            //         'exchangeDetail.member_address': null
            //     })
            //     return
            // }

            if (exchangeDetail.member_address.id !== data.member_address.id) {
                return
            }
            // if (exchangeDetail.member_address.id == data.member_address.id) {
            //     return
            // }
            ajaxPost('member/address', {}, res => {
                var data = res.data.dataList;
                for (var i in data) {
                    if (data[i].id !== exchangeDetail.member_address.id) {
                        this.setData({
                            'exchangeDetail.member_address': null
                        })
                        return
                    } else {
                        return
                    }
                }
            })






        })
    },
    /**
     * 选择配送还是到店取货
     * @param {*} e 
     */
    bindPickerChange: function(e) {
        var data = this.data.exchangeDetail;
        console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
            index: e.detail.value
        })
        data.goods.money = Number(data.goods.money);
        data = data
        if (this.data.index == "1") {
            this.setData({
                exchangeDetail: data,
                isMail: 2,
                youfei: 0
            })
            return
        }
        this.setData({
            exchangeDetail: data,
            isMail: Number(data.goods.is_mail),
            youfei: Number(data.goods.mail_money)
        })
    },
    /**
     * 同不同意协议
     * @param {*} e 
     */
    agreeSelect() {
        var agree_xieyi = this.data.agree_xieyi;

        this.setData({
            agree_xieyi: !agree_xieyi
        })
    },
    /**
     * 跳转充值页面
     * @param {*} e 
     */
    navRechange() {
        this.setData({
            hidepop: true
        })
        wx.navigateTo({
            url: '/pages/my/investMoney/investMoney'
        })
    },
    /**
     * 跳转规则页面
     * @param {*} e 
     */
    navRule() {
        wx.navigateTo({
            url: '/pages/scoresMall/scoresRules/scoresRules?score=true',
        });

    },
    //微信支付
    wxpay(e) {
        var money = Number(e.currentTarget.dataset.money);
        var integral = Number(e.currentTarget.dataset.integral);
        var mymoney = Number(this.data.mymoney);
        var youfei = Number(this.data.youfei);
        var num = Number(this.data.num)
        integral = integral * num
        money = money * num + youfei
        let data = {
            amount: money 
        }
        ajaxPost('weixin/pay', data, res => {
            if (res.code == 0) {
                wx.requestPayment({
                    timeStamp: res.data.timestamp,
                    nonceStr: res.data.nonceStr,
                    package: res.data.package,
                    signType: res.data.signType,
                    paySign: res.data.paySign,
                    success: res => {
                        console.log(res)

                        ajaxPost('weixin/save_money', data, res => {
                            if (res.code == 0) {
                                this.setData({
                                    hidepop: true
                                })
                                let data = {
                                    total_sum: money,
                                    goods_id: this.data.goodId,
                                    address_id: 0,
                                    num: this.data.num,
                                    point: integral
                                }
                                if (this.data.index == "0") {
                                    data = {
                                        total_sum: money,
                                        goods_id: this.data.goodId,
                                        address_id: this.data.exchangeDetail.member_address.id,
                                        num: this.data.num,
                                        point: integral
                                    }
                                }
                                ajaxPost('goods/save_goods', data, res => {
                                    if (res.code == 0) {
                                        showToast('text', "兑换成功！")
                                        setTimeout(() => {
                                            wx.redirectTo({
                                                url: '/pages/my/exchangeSucess/exchangeSucess'
                                            })
                                        }, 2000)
                                    } else if (res.code == 80001) {
                                        this.setData({
                                            hidepop: false,
                                            mymoney: mymoney
                                        })
                                    } else {
                                        showToast('text', res.msg)
                                    }
                                })
                            }
                        })
                    },
                    fail: res => {
                        console.log(res)
                    }
                })
            }
        })


    }
})