// pages/index/appraise/appraise.js
var app = getApp()
import {
    ajaxPost,
    showToast
} from '../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        id: 0,
        appraiseDetail: {},
        imgUrl: [],
        radio: 1,
        items: [{
                name: 1,
                value: '好评',
                checked: true
            },
            {
                name: 2,
                value: '差评'
            }
        ]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        console.log(options)
        this.setData({
            id: options.id
        })
        this.getAppraiseDetail(options.evaluateid)
    },

   
   /**
     * 获取商品详情
     * @param {*} e 
     */
    getGoodDetail(id) {
        let data = {
            id: id
        }
        ajaxPost('member/goods_detail', data, res => {
            if (res.code == 0) {
                console.log(res)
                var data = res.data;
                this.setData({
                    goodDetail: data
                })
            }
        })
    },
       /**
     * 获取评价
     * @param {*} e 
     */
    getAppraiseDetail(evaluateid) {
        let data = {
            id: evaluateid
        }
        ajaxPost('member/evaluate', data, res => {
            if (res.code == 0) {
                console.log(res)
                var data = res.data;
                this.setData({
                    appraiseDetail: data
                })
            }
        })
    },

    radioChange(e) {
        console.log(e)
        this.setData({
            radio: e.detail.value
        })
    },
    /**
 * 跳转商品详情
 * @param {*} e 
 */
    navGoodDetail(e) {
        wx.navigateTo({
            url: '/pages/scoresMall/good_detail/good_detail?id=' + e.currentTarget.dataset.id,
        })
    },


})