//index.js
//获取应用实例
const app = getApp()
import {
    ajaxPost,
    showToast,
    formatTime
} from '../../../utils/util.js'
Page({
    data: {
        navList: [{
            icon: 'https://ruiyuanhui.3todo.com/uploads/img/jifen_mall.png',
            text: "积分商城",
            url: "/pages/scoresMall/index/index"
        }, {
            icon: "https://ruiyuanhui.3todo.com/uploads/img/group_chanye.png",
            text: "集团产业",
            url: "/pages/index/group/index/index"
        }, {
            icon: "https://ruiyuanhui.3todo.com/uploads/img/member_center.png",
            text: "会员中心",
            url: "/pages/my/index/index"
        }, {
            icon: "https://ruiyuanhui.3todo.com/uploads/img/news.png",
            text: "新闻资讯",
            url: "/pages/index/news/news"
        }],
        swiperCurrent: 0,
        isLogin: true,
        is_sign: '立即签到',
        jinyong:true,
        currentIndex:0,
        timestamp: 0,
        actEndTimeList:[]
    },
    onLoad: function(options) {
        if (options.share) {
            wx.setStorageSync('uid', options.id)
        }

        this.getIndexData()

    },
    onShow() {
        var tmp = Date.parse(new Date()).toString();
        tmp = tmp.substr(0, 10);
        this.setData({
            timestamp: Number(tmp)
        })
        var token = wx.getStorageSync("token")
        this.getIndexData()
        if (token) {
            this.setData({
                isLogin:true
            })
            if (this.data.isLogin) {
                ajaxPost('member/index', {}, res => {
                    if (res.code == 60000) {
                        wx.removeStorageSync('token')
                        return
                    }
                })
                ajaxPost('sign_in/index', {}, res => {
                    if (res.code == 0) {
                        console.log(res)
                        var data = res.data.member;
                        if (data.is_sign == 1) {
                            this.setData({
                                is_sign: '已签到'
                            })
                            return
                        } else {
                            this.setData({
                                is_sign: '立即签到'
                            })
                            return
                        }
                    }
                })
            }
        }
    },
       /**
     * 获取首页数据
     * @param {*} e 
     */
    getIndexData() {
        let endTimeList  = [];
        ajaxPost('index/index', {}, res => {
            if(res.code == 0){
                var data = res.data
                for(var i = 0;i<data.new_goods.length;i++){
                    data.new_goods[i].daojishi = formatTime(new Date(data.new_goods[i].end_time*1000))
                    
                }
                data.new_goods.forEach(o=>{
                    endTimeList.push(o.daojishi)
                })
                this.setData({
                    jinyong: true,
                    bannerUrls: data.album,
                    information_news: data.information_news,
                    goodsList: data.goods,
                    consultation_news: data.consultation_news,
                    new_goods: data.new_goods,
                    actEndTimeList: endTimeList
                })
                this.countDown()
            }
           
        })
    },
   
       /**
     * 跳转tab的页面
     * @param {*} e 
     */
    navPage(e) {
        console.log(e)
        var url = e.currentTarget.dataset.url;
        var index = e.currentTarget.dataset.index;
        if (index == 0 || index == 2) {
            wx.switchTab({
                url: url,
            });
            return
        }
        wx.navigateTo({
            url: url,
        });

    },
       /**
     * 跳转h5
     * @param {*} e 
     */
    navh5(e) {
        var url = e.currentTarget.dataset.url;
        if (url.indexOf('/pages/') != -1){
            wx.navigateTo({
                url: url
            })
        }else{
            wx.navigateTo({
                url: '/pages/h5/h5?url=' + url
            })
        }
        
    },
       /**
     * 跳转新闻页面
     * @param {*} e 
     */
    navNewsDetail(e) {
        wx.navigateTo({
            url: '/pages/index/news_detail/news_detail?id=' + e.currentTarget.dataset.id,
        });

    },
       /**
     * 跳转签到页面
     * @param {*} e 
     */
    navSignin() {
        var token = wx.getStorageSync("token");
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        } else {
            wx.navigateTo({
                url: '/pages/scoresMall/getScores/getScores'
            })
        }
    },
       /**
     * 跳转商品详情页面
     * @param {*} e 
     */
    navGoodDetail(e) {
        wx.navigateTo({
            url: '/pages/scoresMall/good_detail/good_detail?id=' + e.currentTarget.dataset.id,
        });
    },
       /**
     * 跳转积分兑换页面
     * @param {*} e 
     */
    navExchange(e) {
        var token = wx.getStorageSync("token");
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        } else {
            console.log(e)
            wx.navigateTo({
                url: '/pages/index/scoreExchange/scoreExchange?id=' + e.currentTarget.dataset.id
            })
        }

    },
       /**
     * 轮播图的指示点
     * @param {*} e 
     */
    swiperChange: function(e) {
        this.setData({
            swiperCurrent: e.detail.current
        })
    },
    timeFormat(param) { //小于10的格式化函数
        return param < 10 ? '0' + param : param;
    },
       /**
     * 查看更多商品
     * @param {*} e 
     */
    lookMoreGood(e) {
        var index = e.currentTarget.dataset.index;

        wx.navigateTo({
            url: '/pages/index/goodsList/goodsList?index='+index,
        })

    },
    countDown() { //倒计时函数
        // 获取当前时间，同时得到活动结束时间数组
        let newTime = new Date().getTime();
        let actEndTimeList = this.data.actEndTimeList;
        let countDownArr = [];
        let timeGoods = this.data.timeGoods;

        // 对结束时间进行处理渲染到页面
        actEndTimeList.forEach(o => {
            let endTime = new Date(o).getTime();
            
            let obj = null;
            // 如果活动未结束，对时间进行处理
            if (endTime - newTime > 0) {
                let time = (endTime - newTime);
                time = time / 1000;
                // 获取天、时、分、秒
                let day = parseInt(time / (60 * 60 * 24));
                let hou = parseInt(time % (60 * 60 * 24) / 3600);
                let min = parseInt(time % (60 * 60 * 24) % 3600 / 60);
                let sec = parseInt(time % (60 * 60 * 24) % 3600 % 60);
                obj = {
                    day: this.timeFormat(day),
                    hou: this.timeFormat(hou),
                    min: this.timeFormat(min),
                    sec: this.timeFormat(sec)
                }
                
            } else { //活动已结束，全部设置为'00'
                // obj = {
                //     day: '00',
                //     hou: '00',
                //     min: '00',
                //     sec: '00'
                // }

                this.getDataList()
            }
            countDownArr.push(obj)
            // timeGoods.forEach(o=>{
            //     o.date = obj
            // })
            // countDownArr.push(obj);
            for(let i = 0;i<timeGoods.length;i++){
                timeGoods[i].date = countDownArr[i]
            }
            // timeGoods.push(obj)
        })
        // 渲染，然后每隔一秒执行一次倒计时函数
        this.setData({
            timeGoods: timeGoods
        })
        setTimeout(this.countDown, 1000);
    },
       /**
     * 暂不登录
     * @param {*} e 
     */
    noLogin() {
        this.setData({
            isLogin: true
        })
    },
       /**
     * 跳转引导页
     * @param {*} e 
     */
    navGuide(){
        wx.navigateTo({
            url: '/pages/guide/guide'
        })
    },
    navNews(e){
        var id = e.currentTarget.dataset.index;
        wx.navigateTo({
            url: '/pages/index/news/news?id='+id
        })
    },
    onShareAppMessage(){},
    swiperChange(e){
        this.setData({
            currentIndex:e.detail.current
        })
    }
})