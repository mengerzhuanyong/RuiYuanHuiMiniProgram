// pages/group/index/index.js
import { ajaxPost, showToast } from '../../../../utils/util';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        cateList: []
    },


    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        this.getCateList();
    },


    /**
  * 获取产业分类
  * @param {*} e 
  */
    getCateList() {
        ajaxPost('index/industry', {}, res => {
            console.log(res)
            var data = res.data
            this.setData({
                cateList: data
            })
        })
    },
    
    /**
  * 跳转产业详情
  * @param {*} e 
  */
    navDetail(e) {
        wx.navigateTo({
            url: '/pages/index/group/companyList/companyList?id=' + e.currentTarget.dataset.id,
        })
    }
})