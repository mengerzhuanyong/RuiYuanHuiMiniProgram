// pages/index/group/companyList/companyList.js
import { ajaxPost, showToast } from '../../../../utils/util';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        limit: 10,
        page: 0,
        companyList: [],
        id: 0,
        doGet: false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            id: options.id
        })
        this.getCateList();
    },
    /**
  * 获取产业分类
  * @param {*} e 
  */
    getCateList() {
        var companyList = this.data.companyList;
        if (this.data.doGet) {
            wx.hideLoading()
            return showToast('text', '暂无新数据')
        }
        let data = {
            page: ++this.data.page,
            limit: this.data.limit,
            id: this.data.id
        }
        ajaxPost('index/industry_list', data, res => {
            if (res.code == 0) {
                console.log(res)
                var data = res.data.list
                if (data.length == 0) {
                    this.setData({
                        doGet: true
                    })
                    wx.hideLoading()
                    return showToast('text', '暂无新数据')

                } else {
                    let arr = companyList.concat(data);
                    this.setData({
                        companyList: arr
                    })
                }
                wx.hideLoading()
            } else {
                wx.hideLoading()
                showToast('text', res.msg)
            }

        })
    },
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        wx.showLoading({
            title: "加载中"
        })
        this.getCateList();
        
    },
   //拨打电话
   callPhone(e){
    var tel = e.currentTarget.dataset.tel;
    wx.makePhoneCall({
        phoneNumber: tel,
    })
},
//获取定位
getAddress(e){
    var latitude = Number(e.currentTarget.dataset.lat);
    var longitude = Number(e.currentTarget.dataset.lng);
    var name = e.currentTarget.dataset.name;
    var address = e.currentTarget.dataset.address;
    wx.openLocation({
        latitude: latitude,
        longitude: longitude,
        name: name,
        address: address
    })
},
//跳转公司详情
    navDetail(e){
        wx.navigateTo({
            url: '/pages/index/group/detail/detail?id='+e.currentTarget.dataset.id
        })
    }
})