// pages/group/detail/detail.js
var app = getApp()
import {ajaxPost,showToast} from '../../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        industryData:{},
        isLogin:true,
        swiperCurrent: 0,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getIndustryData(options.id)
    },



    //拨打电话
    callPhone(e){
        var tel = e.currentTarget.dataset.tel;
        wx.makePhoneCall({
            phoneNumber: tel,
        })
    },
    //获取定位
    getAddress(e){
        var latitude = Number(e.currentTarget.dataset.latitude);
        var longitude = Number(e.currentTarget.dataset.longitude);
        var name = e.currentTarget.dataset.name;
        var address = e.currentTarget.dataset.address;
        wx.openLocation({
            latitude: latitude,
            longitude: longitude,
            name: name,
            address: address
        })
    },
       /**
     * 获取项目信息
     * @param {*} e 
     */
    getIndustryData(id){
        let data = {
            id : id
        }
        ajaxPost('index/industry_detail',data,res=>{
            console.log(res)
            var data = res.data;
            this.setData({
                industryData:data
            })
        })
    },

       /**
     * 跳转商品详情
     * @param {*} e 
     */
    navGoodDetail(e){
        wx.navigateTo({
            url: '/pages/scoresMall/good_detail/good_detail?id='+e.currentTarget.dataset.id,
        })
    },
    //跳转到公司信息
    navCompanyList(e){
        var id = e.currentTarget.dataset.id;
        wx.navigateTo({
            url:'/pages/index/group/companyList/companyList?id=' + id
        })
    },
       /**
     * 跳转新闻详情
     * @param {*} e 
     */
    navNewsDetail(e){
        wx.navigateTo({
            url: '/pages/index/news_detail/news_detail?id=' + e.currentTarget.dataset.id,
        })
    },
       /**
     * 跳转积分兑换页面
     *  {*} e 
     */
    navExchange(e) {
        var token = wx.getStorageSync("token");
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        } else {
            console.log(e)
            wx.navigateTo({
                url: '/pages/index/scoreExchange/scoreExchange?id=' + e.currentTarget.dataset.id
            })
        }

    },
       /**
     * 跳转引导页
     * @param {*} e 
     */
    navGuide() {
        wx.navigateTo({
            url: '/pages/guide/guide'
        })
        this.setData({
          isLogin:true
        })
    },
       /**
     * 暂不登录
     * @param {*} e 
     */
    noLogin() {
        this.setData({
            isLogin: true
        })
    },
       /**
     * 轮播图切换
     * @param {*} e 
     */
    swiperChange: function(e) {
        console.log(e)
        this.setData({
            swiperCurrent: e.detail.current
        })
    },
    navNews(e){
        var id = e.currentTarget.dataset.index;
        wx.navigateTo({
            url: '/pages/index/news/news?id='+id
        })
    }
})