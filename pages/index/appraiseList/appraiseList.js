// pages/index/appraiseList/appraiseList.js
import {
    ajaxPost,
    showToast
} from '../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        appraiseList: [],
        tabList: ['好评', '差评'],
        tabCurrent: 0,
        id: '',
        page: 0,
        limit: 10,
        noGet: false,
        index:0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.setData({
            id: options.id
        })
        this.getAppraiseList(options.id, this.data.tabCurrent + 1)
    },


    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {
        wx.showLoading({
            title: '加载中',
        })
        this.getAppraiseList(this.data.id, this.data.tabCurrent + 1)
    },


       /**
     * 获取评价列表
     * @param {*} e 
     */
    getAppraiseList(id, state) {
        var appraiseList = this.data.appraiseList;
        var tabCurrent = this.data.tabCurrent;
        
        if (this.data.noGet) {
            wx.hideLoading()
            return
        }
        
        let data = {
            goods_id: id,
            state: state,
            limit: this.data.limit,
            page: ++this.data.page
        }

        ajaxPost('goods/evaluate_list', data, res => {
            console.log(res)
            if (res.code == 0) {
                var data = res.data.list;
                if (data.length == 0) {
                    this.setData({
                        noGet: true
                    })
                    wx.hideLoading()
                    return
                } else {
                    // var reg = /^(\d{3})\d{4}(\d{4})$/;
                    // for(var i in data){
                    //     data[i].phone.replace(reg, '$1****$2')
                    // }
                    // data = data
                    let arr = appraiseList.concat(data)
                    this.setData({
                        appraiseList: arr
                    })

                }
                wx.hideLoading()
            }
        })
    },
    /**
     * tab切换
     */
    tabChange(e) {
        var index = e.currentTarget.dataset.index;
        this.setData({
            tabCurrent:index,
            appraiseList:[],
            noGet:false
        })
        var id = index + 1
        var appraiseList = this.data.appraiseList;
        var tabCurrent = this.data.tabCurrent;

                  

        let data = {
            goods_id: this.data.id,
            state: id,
            limit: this.data.limit,
            page: 1
        }

        ajaxPost('goods/evaluate_list', data, res => {
            console.log(res)
            if (res.code == 0) {
                var data = res.data.list;
                if (data.length == 0) {
                    this.setData({
                        noGet: true
                    })
                    wx.hideLoading()
                    return
                } else {
                    // var reg = /^(\d{3})\d{4}(\d{4})$/;
                    // for (var i in data) {
                    //     data[i].phone.replace(reg, '$1****$2')
                    // }
                    // data = data
                    let arr = appraiseList.concat(data)
                    this.setData({
                        appraiseList: arr
                    })

                }
                wx.hideLoading()
            }
        })
    },
       /**
     * 预览图片
     * @param {*} e 
     */
    previewImage(e) {
        var index = e.currentTarget.dataset.index;
        var urls = e.currentTarget.dataset.urls;
        wx.previewImage({
            urls: urls,
            current: urls[index]
        })
    }
})