// pages/index/news_detail/news_detail.js
var app = getApp()
import {
    ajaxPost,
    showToast
} from '../../../utils/util.js';
var WxParse = require('../../../wxParse/wxParse.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        isLogin: true,
        newsDetail: {}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {

        this.getNewsDetail(options.id)
    },



    /**
     * 获取新闻详情
     */

    getNewsDetail(id) {
        let data = {
            id: id
        }
        ajaxPost('news/detail', data, res => {
            if(res.code == 0){
                var data = res.data;
                this.setData({
                    newsDetail: data
                })
                var article = data.content;
                /**
                 * WxParse.wxParse(bindName , type, data, target,imagePadding)
                 * 1.bindName绑定的数据名(必填)
                 * 2.type可以为html或者md(必填)
                 * 3.data为传入的具体数据(必填)
                 * 4.target为Page对象,一般为this(必填)
                 * 5.imagePadding为当图片自适应是左右的单一padding(默认为0,可选)
                 */
                let content = app.towxml.toJson(article, 'html');
                this.setData({
                    content: content
                })
                // WxParse.wxParse('article', 'html', article, this, 5);
            }else{
                wx.showToast({
                    title: '数据错误',
                })
            }
            
        })
    },
    like() {
        var newsDetail = this.data.newsDetail;
        if (newsDetail.is_like == 0) {
            newsDetail.sum_like = newsDetail.sum_like + 1
            newsDetail.is_like = 1
        } else {
            newsDetail.sum_like = newsDetail.sum_like - 1
            newsDetail.is_like = 0
        }
       
        let data = {
            id: this.options.id
        }
        ajaxPost('news/like', data, res => {

            if (res.code == 40099) {

                showToast('text', res.msg)
                this.setData({
                    newsDetail: newsDetail
                })
            }
        })
    },
      /**
     * 跳转商品详情页面
     * @param {*} e 
     */
    navGoodDetail(e) {
        wx.navigateTo({
            url: '/pages/scoresMall/good_detail/good_detail?id=' + e.currentTarget.dataset.id,
        });
    },
       /**
     * 跳转积分兑换页面
     * @param {*} e 
     */
    navExchange(e) {
        var token = wx.getStorageSync("token");
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        } else {
            console.log(e)
            wx.navigateTo({
                url: '/pages/index/scoreExchange/scoreExchange?id=' + e.currentTarget.dataset.id
            })
        }

    },
})