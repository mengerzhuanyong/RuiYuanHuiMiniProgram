// pages/myScores/myScores.js
import {
    showToast,
    ajaxPost
} from '../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        scoreList: {},
        doGet: false,
        page: 0,
        limit: 10,
        jinyong: true,
        chooseTime: ['最近一个月', '最近三个月','最近半年'],
        index:0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        if (this.data.noGet) {
            wx.hideLoading()
            return;
        }
        let data = {
            page: ++this.data.page,
            limit: this.data.limit,
            moon:1
        };
        ajaxPost('member/point_log', data, res => {
            console.log(res)
            var data = res.data;
            if (res.code == 0) {
                this.setData({
                    scoreList:data
                })
            
            }else if(res.code == 40020){
                this.setData({
                    jinyong:false
                })
            }
        })
    },

 

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {
        wx.showLoading({
            title: '加载中',
        })
        this.getScoresList()
    },

    //跳转到积分规则
    navScoreRule() {
        wx.navigateTo({
            url: '/pages/scoresMall/scoresRules/scoresRules',
        })
    },
       /**
     * 获取积分记录
     * @param {*} e 
     */
    getScoresList() {
        var scoreList = this.data.scoreList;
        if (this.data.noGet) {
            wx.hideLoading()
            return;
        }
        let data = {
            page: ++this.data.page,
            limit: this.data.limit,
            moon:1
        };
        ajaxPost('member/point_log', data, res => {
            console.log(res)
            var data = res.data.list
            if (res.code == 0) {
                if (data.length == 0) {
                    this.setData({
                        noGet: true
                    })
                } else {
                    var data = res.data;
                    let newarr = scoreList.list.concat(data.list)
                    this.setData({
                        'scoreList.list': newarr
                    })
                }
                wx.hideLoading()

            }
        })
    },
       /**
     * 跳转积分商城
     * @param {*} e 
     */
    navScrollMall(){
        wx.switchTab({
            url: '/pages/scoresMall/index/index'
        })
    },
    bindPickerChange: function (e) {
        console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
            index: Number(e.detail.value)
        })
        var scoreList = this.data.scoreList;
        
        let data = {
            page: 1,
            limit: this.data.limit,
            moon: this.data.index + 1
        };
        ajaxPost('member/point_log', data, res => {
            console.log(res)
            this.setData({
                'scoreList.list':[]
            })
            if (res.code == 0) {
                var data = res.data.list
                
                    this.setData({
                        'scoreList.list': data,
                        noGet:false
                    })
                
                wx.hideLoading()

            }
        })
    },
})