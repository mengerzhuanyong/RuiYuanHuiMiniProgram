// pages/scoresMall/good_detail/good_detail.js
var app = getApp()
import {
    ajaxPost,
    showToast,
    formatTime
} from '../../../utils/util';
var WxParse = require('../../../wxParse/wxParse.js');
let videoid = null;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        isCollect: false,
        goodDetail: {},
        isLogin: true,
        goods_id: '',
        jinyong: true,
        uid: '',
        isPlay: false,
        timestamp: 0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.videoid = wx.createVideoContext('video')
        if (options.share) {
            wx.setStorageSync('uid', options.uid)
        }
        this.setData({
            goods_id: options.id
        })
        this.getGoodDetail(options.id)

    },

    onShow() {
        var tmp = Date.parse(new Date()).toString();
        tmp = tmp.substr(0, 10);
        this.setData({
            timestamp: Number(tmp)
        })
        var token = wx.getStorageSync("token");
        if (token) {
            this.setData({
                isLogin: true
            })
            ajaxPost('member/index', {}, res => {
                if (res.code == 0) {
                    var data = res.data.member;
                    this.setData({
                        uid: data.id
                    })
                }
            })
            this.getGoodDetail(this.options.id)
        }
    },
    playVideo() {
        this.setData({
            isPlay: true
        })
    },
    pauseVideo() {

        this.setData({
            isPlay: false
        })



    },
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {
        var image = this.data.goodDetail.goods_detail.image;
        var name = this.data.goodDetail.goods_detail.name
        return {
            title: name,
            image: image,
            path: "/pages/scoresMall/good_detail/good_detail?id=" + this.data.goods_id + '&uid=' + this.data.uid + '&share=true'
        }
    },
    //收藏功能
    collect() {
        var isCollect = this.data.isCollect;
        var token = wx.getStorageSync("token");
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        } else {

            isCollect = !isCollect;
            let data = {
                goods_id: this.data.goods_id
            }
            // if (isCollect) {
            ajaxPost('member/collect', data, res => {
                console.log(res)
                if (res.code == 0) {
                    showToast('text', res.msg)
                    this.setData({
                        isCollect: isCollect
                    })
                } else {
                    showToast('text', res.msg)
                }
            })
            return
            // }
            // ajaxPost('member/cancel_collect', data, res => {
            //     console.log(res)
            //     if (res.code == 0) {
            //         showToast('text', res.msg)
            //         this.setData({
            //             isCollect: false
            //         })
            //     } else {
            //         showToast('text', res.msg)
            //     }
            // })

        }
    },
    //获取详情
    getGoodDetail(id) {
        let data = {
            id: id
        }
        ajaxPost('Goods/goods_detail', data, res => {
            console.log(res)
            if (res.code == 0) {
                var data = res.data;
                // var reg = /^(\d{3})\d{4}(\d{4})$/;

                // data.evaluate.phone.replace(reg, '$1****$2')

                // data = data
                if (data.goods_detail.video) {
                    data.goods_detail.goods_image.splice(0, 0, {
                        video: data.goods_detail.video
                    })

                }
                data.goods_detail.end_time = formatTime(new Date(data.goods_detail.end_time * 1000))
                this.setData({
                    goodDetail: data,
                    isCollect: data.goods_collect,
                    jinyong: true
                })

                this.countTime()
                var article = data.goods_detail.details;
                /**
                 * WxParse.wxParse(bindName , type, data, target,imagePadding)
                 * 1.bindName绑定的数据名(必填)
                 * 2.type可以为html或者md(必填)
                 * 3.data为传入的具体数据(必填)
                 * 4.target为Page对象,一般为this(必填)
                 * 5.imagePadding为当图片自适应是左右的单一padding(默认为0,可选)
                 */

                WxParse.wxParse('article', 'html', article, this, 5);
            } else if (res.code == 40020) {
                this.setData({
                    jinyong: false
                })
                return showToast('text', res.msg)
            }

        })
    },
    countTime() {
        var date = new Date();
        var now = date.getTime();
        var endDate = new Date(this.data.goodDetail.goods_detail.end_time); //设置截止时间
        var end = endDate.getTime();
        var leftTime = (end - now); //时间差   
        if(end == 0){
            return
        }
        leftTime = leftTime;
        var d, h, m, s, ms;
        if (leftTime >= 0) {
            d = Math.floor(leftTime / 1000 / 60 / 60 / 24);
            h = Math.floor(leftTime / 1000 / 60 / 60 % 24);
            m = Math.floor(leftTime / 1000 / 60 % 60);
            s = Math.floor(leftTime / 1000 % 60);
            s = s < 10 ? "0" + s : s
            m = m < 10 ? "0" + m : m
            h = h < 10 ? "0" + h : h
            this.setData({
                countdown: d + "天" + h + "时" + m + "分" + s +'秒',
            })
            //递归每秒调用countTime方法，显示动态时间效果
            setTimeout(
                this.countTime, 1000);
        } else {
            this.getGoodDetail(this.options.id)
        }

    },

    /**
     * 跳转兑换
     * @param {*} e 
     */
    navExchange(e) {
        var token = wx.getStorageSync("token");
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        } else {
            if (this.data.goodDetail.goods_detail.stock_num == 0) {
                return showToast('text', '库存已为0,无法兑换。')
            }
            wx.navigateTo({
                url: '/pages/index/scoreExchange/scoreExchange?id=' + e.currentTarget.dataset.id
            });
        }

    },
    /**
     * 跳转评价列表
     * @param {*} e 
     */
    navAppraiseList(e) {
        var id = e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '/pages/index/appraiseList/appraiseList?id=' + id
        });

    },
    /**
     * 暂不登录
     * @param {*} e 
     */
    noLogin() {
        this.setData({
            isLogin: true
        })
    },
    /**
     * 跳转引导页
     * @param {*} e 
     */
    navGuide() {
        wx.navigateTo({
            url: '/pages/guide/guide'
        })
    },
    /**
     * 预览图片
     * @param {*} e 
     */
    previewimage(e) {
        var urls = e.currentTarget.dataset.urls;
        var index = e.currentTarget.dataset.index;
        wx.previewImage({
            current: urls[index],
            urls: urls
        })
    },
    swiperChange(e) {
        if (e.detail.current == 0) {
            if (this.data.isPlay) {
                this.videoid.play()
            }

        } else {
            this.videoid.pause()
        }
    }
})