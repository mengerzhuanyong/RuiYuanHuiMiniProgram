// pages/getScores/getScores.js
import {ajaxPost,showToast} from '../../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        swiperCurrent: 0,
        signin:{},
        jinyong: true,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getIndexData()
    },

   

    /**
     * 跳转新闻详情
     */

    
    navNewsDetail(e) {
        var url = e.currentTarget.dataset.url;
        wx.navigateTo({
            url: url,
        })
    },
       /**
     * 轮播图变化
     * @param {*} e 
     */
    swiperChange: function (e) {
        console.log(e)
      this.setData({
        swiperCurrent: e.detail.current
      })
    },
       /**
     * 获取页面数据
     * @param {*} e 
     */
    getIndexData() {
        ajaxPost('sign_in/index', {}, res => {
            if(res.code == 0){
                var data = res.data
                this.setData({
                    timegoods: data.goods_list,
                    yewu_news: data.yewu_news,
                    goodsList: data.goods,
                    signin: data
                })
            }else if(res.code == 40020){
                this.setData({
                    jinyong:false
                })
            }
           
        })
    },
       /**
     * 跳转商品详情
     * @param {*} e 
     */
    navGoodDetail(e) {
        wx.navigateTo({
            url: '/pages/scoresMall/good_detail/good_detail?id=' + e.currentTarget.dataset.id
        });

    },
       /**
     * 查看更多商品
     * @param {*} e 
     */
    lookMoreGood(e) {
        var index = e.currentTarget.dataset.index;

        wx.navigateTo({
            url: '/pages/index/goodsList/goodsList?index=' + index,
        })

    },
       /**
     * 签到
     * @param {*} e 
     */
    signIn(){
        ajaxPost('sign_in/sign_in',{},res=>{
            if(res.code == 0){
                showToast('success',res.msg)
                this.getIndexData();
            }else{
                showToast('text', res.msg)
            }
        })
    },
       /**
     * 跳转兑换
     * @param {*} e 
     */
    navExchange(e) {
        var token = wx.getStorageSync("token");
        if (!token) {
            this.setData({
                isLogin: false
            })
            return
        } else {
            console.log(e)
            wx.navigateTo({
                url: '/pages/index/scoreExchange/scoreExchange?id=' + e.currentTarget.dataset.id
            })
        }

    },
       /**
     * 跳转积分
     * @param {*} e 
     */
    navMyscore(){
        wx.navigateTo({
            url: '/pages/scoresMall/myScores/myScores',
        })
    },
    navNews(e){
        var id = e.currentTarget.dataset.index;
        wx.navigateTo({
            url: '/pages/index/news/news?id='+id
        })
    }
})