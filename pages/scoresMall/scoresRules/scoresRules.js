// pages/pointRules/pointRules.js
var app = getApp()
import {ajaxPost,showToast} from '../../../utils/util.js'
var WxParse = require('../../../wxParse/wxParse.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        score:false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        if (options.score){
            this.getXieyi()
            wx.setNavigationBarTitle({
                title: '会员须知',
            })
            this.setData({
                score:true
            })
            return
        }
        this.getRule()
    },


   /**
     * 获取规则
     * @param {*} e 
     */
    getRule(){
        ajaxPost('rule/rule_index',{},res=>{
            if(res.code == 0){
                var data = res.data;
                var article = data;
                /**
                 * WxParse.wxParse(bindName , type, data, target,imagePadding)
                 * 1.bindName绑定的数据名(必填)
                 * 2.type可以为html或者md(必填)
                 * 3.data为传入的具体数据(必填)
                 * 4.target为Page对象,一般为this(必填)
                 * 5.imagePadding为当图片自适应是左右的单一padding(默认为0,可选)
                 */

                WxParse.wxParse('article', 'html', article, this, 5);
            }
        })
    },
       /**
     * 获取协议
     * @param {*} e 
     */
    getXieyi(){
        ajaxPost('rule/agreement_index', {}, res => {
            if (res.code == 0) {
                var data = res.data;
                var article = data.detail;
                this.setData({
                    rule:data
                })
                /**
                 * WxParse.wxParse(bindName , type, data, target,imagePadding)
                 * 1.bindName绑定的数据名(必填)
                 * 2.type可以为html或者md(必填)
                 * 3.data为传入的具体数据(必填)
                 * 4.target为Page对象,一般为this(必填)
                 * 5.imagePadding为当图片自适应是左右的单一padding(默认为0,可选)
                 */

                WxParse.wxParse('article', 'html', article, this, 5);
            }
        })
    }
})