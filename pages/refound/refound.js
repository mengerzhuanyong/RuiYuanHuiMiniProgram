// pages/index/appraise/appraise.js
var app = getApp()
import {
    qiniuUpload,
    ajaxPost,
    showToast
} from '../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        id: 0,
        goodDetail: {},
        imgUrl: [],
        radio: 1,
        imgs:'',
        items: [{
                name: 1,
                value: '好评',
                checked: true
            },
            {
                name: 2,
                value: '差评'
            }
        ]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        console.log(options)
        this.setData({
            id: options.id
        })
        this.getGoodDetail(options.id)
    },

    


    /**
     * 获取商品详情
     * @param {} id 
     */
    getGoodDetail(id) {
        let data = {
            id: id
        }
        ajaxPost('member/goods_detail', data, res => {
            if (res.code == 0) {
                console.log(res)
                var data = res.data;
                this.setData({
                    goodDetail: data
                })
            }
        })
    },

    /**
     *  上传单张图片公共方法
     * @param {*} e  穿参
     */
    businessUpload() {
        var arr = []
        var that = this;
        wx.chooseImage({
            count: 9,
            sizeType: ['original', 'compressed'],
            sourceType: ['album', 'camera'],
            success(res) {
                
                qiniuUpload.upload(res.tempFilePaths, res => {
                    
                    console.log(res)
                    let imgUrl = that.data.imgUrl
                    for (let i = 0; i < res.length; i++) {
                        imgUrl.push(app.globalData.QINIU_IMG_URL + res[i].key)
                    }
                    that.setData({
                        imgUrl: imgUrl,
                        imgs:imgUrl.join('|')
                    })
                })
            }
        })
    },

       /**
     * 提交评价
     * @param {*} e 
     */
    bindSubmit(e) {
        console.log(e)
        var evaluate = e.detail.value.evaluate;
        var imgs = this.data.imgs;
        var imgUrl = this.data.imgUrl;
        if (evaluate.length == 0 && imgUrl.length == 0) {
            return showToast('text', '请输入内容或提交图片')
        }
        let data = {
            image: imgs,
            content: evaluate,
            id: this.data.goodDetail.goods_pay_id
        }
        ajaxPost('member/refound', data, res => {
            if (res.code == 0) {

                showToast('text', res.msg)

                setTimeout(() => {
                    wx.navigateBack({
                        delta: 1,
                    })
                }, 1000)
            }
        })
    },
       /**
     * 删除上传的图片
     * @param {*} e 
     */
    delImg(e) {
        var imgUrl = this.data.imgUrl;
        var index = e.currentTarget.dataset.index;
        imgUrl.splice(index, 1);
        this.setData({
            imgUrl: imgUrl,
            imgs:imgUrl.join('|')
        })
    },
       /**
     * 预览上传的图片
     * @param {*} e 
     */
    previewImg(e){
        var index = e.currentTarget.dataset.index;
        var imgUrl = this.data.imgUrl;
        wx.previewImage({
            current: imgUrl[index],
            urls: imgUrl
        })
    },
    /**
* 跳转商品详情
* @param {*} e 
*/
    navGoodDetail(e) {
        wx.navigateTo({
            url: '/pages/scoresMall/good_detail/good_detail?id=' + e.currentTarget.dataset.id,
        })
    },

})