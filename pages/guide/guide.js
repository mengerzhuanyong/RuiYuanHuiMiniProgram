// pages/guide/guide.js
import {ajaxPost,showToast} from '../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {

    },



    /**
     * 获取用户信息
     */
    bindGetUserInfo(e) {
        var uid = wx.getStorageSync('uid')

        wx.login({
            success: res => {
                console.log(res)
                console.log(e)
                if (e.detail.iv == undefined) {
                    return
                }
                let data = {
                    code: res.code,
                    iv: e.detail.iv,
                    encryptedData: e.detail.encryptedData
                }
                if (uid.length !== 0) {
                    data = {
                        code: res.code,
                        iv: e.detail.iv,
                        encryptedData: e.detail.encryptedData,
                        uid: uid
                    }
                }
                ajaxPost('Weixin/login', data, res => {
                    if (res.code == 0) {
                        var data = res.data;
                        console.log(res)
                        wx.setStorageSync("token", data.token)
                        wx.navigateBack({
                            delta: 1,
                        })
                        wx.removeStorageSync('uid')
                        if (data.phone == null) {
                            wx.navigateTo({
                                url: '/pages/my/bindTel/bindTel',
                            })
                            return
                        }
                    }
                })
            },
            fail: res => {
                showToast("text", "请授权登录小程序，以便可以体验到更全面的服务")
                console.log(res)
            }
        })
    }
})